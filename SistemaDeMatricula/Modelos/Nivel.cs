﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaDeMatricula.Modelos
{
    class Nivel
    {
        public int Id { get; set; }
        public string NombreDocumento { get; set; }
        public int NumeroNivel { get; set; }
    }
}
