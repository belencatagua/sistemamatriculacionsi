﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaDeMatricula.Modelos
{
    class Documento
    {
        private static Documento documento;

        protected Documento()
        {

        }

        public static Documento Documentos
        {
            get
            {
                if (documento == null)
                {
                    documento = new Documento();
                }
                return documento;
            }
        }
        public int Id { get; set; }
        public string NombreDocumento{ get; set; }
        public int Nivel { get; set; }
    }
}
