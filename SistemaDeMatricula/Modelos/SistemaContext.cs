﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaDeMatricula.Modelos
{
    class SistemaContext : DbContext
    {
        private const string connectionString =
            "Server =.\\SQLEXPRESS; Database = Sistema; Trusted_Connection=true;";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }
        public DbSet<Documento> documentos { get; set; }
        public DbSet<Estudiante> estudiantes { get; set; }
        public DbSet<Nivel> niveles { get; set; }
    }
}
